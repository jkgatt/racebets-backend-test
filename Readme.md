# Racebets Backend Developer Test Task

## Getting Started
* First the database needs to be set up by running 3 query files (all database queries are found in the database>statements folder):
    * First the "create_database_and_table.sql" query needs to be run so that the database and its table will be created.
    * Then the "create_db_user.sql" query needs to be run so that the database user and privaleges are granted on the database created above.
    * Finally the "default_data.sql" query needs to be run, so that the default data is added to the database such as country and gender.
* After the database is set up, the Apache or the PHP server can be started.
    * If you are using Apache, just put the "racebets-app" folder in the htdocs folder
    * An easier way is to use the built-in PHP web server, just by going to the "public" folder under the "racebets-app" folder and running the following command: "php -S localhost:8000"

## Usage
* There are a number of resources with various methods that are described below:

### Country
* GET request on "/country"
    * A list of countries will be retrieved from the database

### Customer
* GET request on "/customer"
    * A list of customers will be retrieved from the database
* POST request on "/customer"
    * Creates a new customer in the database according to the details passed in the body
    * A JSON body needs to be added to request, like the one below:
```
{
        	"first_name": "Jean",
        	"last_name": "Gatt",
        	"email": "jeangatt@gmail.com",
        	"gender": "male",
        	"country": "Malta"
}
```
* GET request on "/customer/{id}"
    * Retrieve a customer details according to the ID passed
* PUT request on "/customer/{id}"
    * Updates a customer according to the details passed in the body.
    * Any combination of customer propeties can be used from the following: first name, last name, email, gender and country.
    * A JSON body needs to be added to request, like the one below:
```
{
        	"gender": "female",
        	"country": "Germany"
}
```
### Account & Transactions
* GET request on "/report"
    * Generates a financial report which is grouped by the country and day of the transaction
* POST request on "/transaction"
    * Add a new transaction to the database, either a deposit or withdrawal
    * A JSON body needs to be added to request, like the one below:
```
{
        	"type" : "Deposit",
    	    "customer_id" : 1,
    	    "amount": 500
}
```

## Other Notes
* A log file can be found in the logs folder with more information.
* Error checking is not completed (will be added tonight), but all the code is completed

