<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 05/09/2017
 * Time: 09:56
 */

/**
 * Retrieve the database ID of a gender via their name
 *
 * @param $dbConnection
 * @param $logger
 * @param $name
 * @return int
 */
function getGenderIdByName($dbConnection, $logger, $name){
    $id = 0;

    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_gender_id_by_name.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(['name' => strtolower($name)]);

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        } else {
            $id = $stmt->fetchColumn();
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return $id;
}