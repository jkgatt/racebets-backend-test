<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 05/09/2017
 * Time: 22:33
 */

/**
 * Total transaction count according to an account ID and transaction type ID
 *
 * @param $dbConnection
 * @param $logger
 * @param $accountId
 * @param $transactionTypeId
 * @return int | count
 */
function getTotalTransactionCountByAccountIdAndTransactionTypeId($dbConnection, $logger, $accountId, $transactionTypeId){

    $count = 0;

    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_total_transaction_count_by_account_id_and_transaction_type.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute([
            "account_id" => $accountId,
            "transaction_type_id" => $transactionTypeId
        ]);

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        }else {
            $count = $stmt->fetchColumn();
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return $count;
}


/**
 * Inserts a transaction into the database
 *
 * @param $dbConnection
 * @param $logger
 * @param transaction
 * @return int
 */
function createTransaction($dbConnection, $logger, $transaction){
    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/insert_transaction.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(
            $transaction
        );

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return 0;
}