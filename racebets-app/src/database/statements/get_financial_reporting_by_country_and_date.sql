SELECT
  transaction.date,
  country.short_name as 'country',
  transaction_type.name as 'transaction_type',
  count(transaction.transaction_type_id) as 'transaction_count',
  sum(transaction.amount) as 'amount'
FROM
  transaction
  INNER JOIN
  transaction_type ON transaction.transaction_type_id = transaction_type.id
  INNER JOIN
  account ON transaction.account_id = account.id
  INNER JOIN
  customer as customer ON account.customer_id = customer.id
  INNER JOIN
  country ON customer.country_id = country.id
GROUP BY transaction.date, country.id, transaction.transaction_type_id;