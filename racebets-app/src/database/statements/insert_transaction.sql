INSERT INTO `transaction`
  (`transaction_type_id`,
   `account_id`,
   `date`,
   `amount`)
VALUES
  (:transaction_type_id,
   :account_id,
   FROM_UNIXTIME(:timestamp),
   :amount);