INSERT INTO customer
(`first_name`,
 `last_name`,
 `email`,
 `bonus_percentage`,
 `gender_id`,
 `country_id`)
VALUES
  (:first_name,
   :last_name,
   :email,
   :bonus_percentage,
   :gender,
   :country);
