UPDATE `account`
SET
  `balance` = :balance,
  `bonus_amount` = :bonus_amount
WHERE `id` = :account_id;