<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 05/09/2017
 * Time: 15:01
 */

require_once __DIR__ . '/transaction.php';

/**
 * Retrieve account details via a customer ID
 *
 * @param $dbConnection
 * @param $logger
 * @param $customerId
 * @return null | account
 */
function getAccountByCustomerId($dbConnection, $logger, $customerId){
    $account = null;

    try {

        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_account_by_customer_id.sql');

        $logger->info("Retrieved the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(["customer_id" => $customerId]);

        if($stmt->errorCode() != 0){
            $logger->error("Statement Error: ". $stmt->errorInfo());
            return null;
        } else {
            $account = $stmt->fetch(PDO::FETCH_OBJ);
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return null;
    } finally {
        $dbConnection = null;
    }
    return $account;
}

/**
 * Create account for a specific customer
 *
 * @param $dbConnection
 * @param $logger
 * @param $customerId
 * @return int
 */
function createAccount($dbConnection, $logger, $customerId){
    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/insert_account.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute([
            "customer_id" => $customerId
        ]);

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return 0;
}

/**
 * Updates the balances of an account
 *
 * @param $dbConnection
 * @param $logger
 * @param $updated
 * @return int
 */
function updateAccount($dbConnection, $logger, $updated){
    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/update_account_balances.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(
            $updated
        );

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return 0;
}

/**
 * Adding a transaction to an account (both deposit and withdrawal)
 *
 * @param $dbConnection
 * @param $logger
 * @param $transactionTypeID
 * @param $parsedBody
 * @return int
 */
function transactionToAccount($dbConnection, $logger, $transactionTypeID, $bonusPercentage, $parsedBody){

    $account = null;

    try{
        $dbConnection->beginTransaction();

        // Retrieving account linked to this customer
        $account = getAccountByCustomerId($dbConnection, $logger, $parsedBody['customer_id']);

        if(is_null($account) || !$account){
            // An account needs to be created for the customer and its details retrieved
            if(createAccount($dbConnection, $logger, $parsedBody['customer_id'])){
                $logger->error("An error has occurred when trying to create a new customer account");
                return -1;
            }

            // Retrieve the account again
            $account = getAccountByCustomerId($dbConnection, $logger, $parsedBody['customer_id']);
        }

        $updatedBonus = $account->bonus_amount;
        $updatedAmount = $account->balance;

        switch(strtolower($parsedBody['type'])) {
            case "deposit":
                $totalNumberOfDeposits = (int)getTotalTransactionCountByAccountIdAndTransactionTypeId($dbConnection,
                    $logger, $account->id, $transactionTypeID);
                if ($totalNumberOfDeposits == -1) {
                    $logger->error("An error has occurred when trying to retrieve the total number of transactions" .
                        "for a specific account and type.");
                    return -1;
                }
                // Adding one to compensate for this deposit
                $totalNumberOfDeposits += 1;
                if ($totalNumberOfDeposits !== 0 && $totalNumberOfDeposits % 3 === 0) {
                    $updatedBonus += $bonusPercentage / 100 * $parsedBody['amount'];
                }
                $updatedAmount += $parsedBody['amount'];
                break;
            case "withdrawal":
                if ($parsedBody['amount'] > $account->balance) {
                    $logger->error("Cannot withdraw more money(" . $parsedBody['amount'] . "), then what is available(" .
                        $account->balance . ").");
                    return -1;
                }
                $updatedAmount -= $parsedBody['amount'];
                break;
            default:
                $logger->error("Transaction type of " . $parsedBody['type'] . " is not available.");
                return -1;
                break;
        }

        // Add transaction to the transaction table
        $transaction = [
            "transaction_type_id" => $transactionTypeID,
            "account_id" => $account->id,
            "timestamp" => time(),
            "amount" => $parsedBody['amount']
        ];

        if(createTransaction($dbConnection, $logger, $transaction) ==  -1){
            $logger->error("An error has occurred when trying to create a new transaction");
            return -1;
        }

        // Update the account balances
        $updatedAccount = [
            "balance" => $updatedAmount,
            "bonus_amount" => $updatedBonus,
            "account_id" => $account->id
        ];

        if(updateAccount($dbConnection, $logger, $updatedAccount) ==  -1){
            $logger->error("An error has occurred when trying to updated the account");
            return -1;
        }

        $dbConnection->commit();
    } catch (PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        $dbConnection->rollback();
        return -1;
    } finally {
        $dbConnection = null;
    }
    return 0;
}

/**
 * Retrieve the data for the financial report
 *
 * @param $dbConnection
 * @param $logger
 * @return null | report data
 */
function getFinancialReport($dbConnection, $logger){
    $report = null;

    try {

        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_financial_reporting_by_country_and_date.sql');

        $logger->info("Retrieved the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute();

        if($stmt->errorCode() != 0){
            $logger->error("Statement Error: ". $stmt->errorInfo());
            return null;
        } else {
            $report = $stmt->fetchAll(PDO::FETCH_OBJ);
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return null;
    } finally {
        $dbConnection = null;
    }
    return $report;
}