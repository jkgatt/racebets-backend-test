<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 05/09/2017
 * Time: 13:34
 */

function executeCustomQuery($dbConnection, $logger, $customQuery){
    try {
        $logger->info("Retrieved and going to execute the following query: ". $customQuery);

        $stmt = $dbConnection->prepare($customQuery);
        $stmt->execute();

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return 0;
}