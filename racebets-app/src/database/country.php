<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 03/09/2017
 * Time: 21:19
 */

/**
 * Retrieves all the countries found in the database
 *
 * @param $dbConnection
 * @param $logger
 * @return Either null if an error occurred or a list of countries
 */
function getAllCountries($dbConnection, $logger) {

    $countries = null;
    try {

        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_all_countries.sql');

        $logger->info("Retrieved the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute();

        if($stmt->errorCode() != 0){
            $logger->error("Statement Error: ". $stmt->errorInfo());
            return null;
        } else {
            $countries = $stmt->fetchAll(PDO::FETCH_OBJ);
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return null;
    } finally {
        $dbConnection = null;
    }
    return $countries;
}

/**
 * Retrieve the database ID of a country via their name
 *
 * @param $dbConnection
 * @param $logger
 * @param $name
 * @return int or -1 if an error occurs
 */
function getCountryIdByName($dbConnection, $logger, $name){
    $id = 0;

    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_country_id_by_name.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(['name' => strtolower($name)]);

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        } else {
            $id = $stmt->fetchColumn();
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return $id;
}