<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 04/09/2017
 * Time: 09:57
 */

/**
 * Retrieves all the customers found in the database
 *
 * @param $dbConnection
 * @param $logger
 * @return Either null if an error occurred or a list of customers
 */
function getAllCustomer($dbConnection, $logger){

    $customers = null;

    try {

        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_all_customers.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute();

        if($stmt->errorCode() != 0){
            $logger->error("Database Error: ". $stmt->errorInfo());
            return null;
        } else {
            $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return null;
    } finally {
        $dbConnection = null;
    }
    return $customers;
}

/**
 * Retrieve a customer by its ID
 *
 * @param $dbConnection
 * @param $logger
 * @param $id
 * @return null if an error occurs, or a customer
 */
function getCustomerById($dbConnection, $logger, $id){

    $customer = null;

    try {

        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_customer_by_id.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(['id' => $id]);

        if($stmt->errorCode() != 0){
            $logger->error("Database Error: ". $stmt->errorInfo());
            return null;
        } else {
            $customer = $stmt->fetch(PDO::FETCH_OBJ);
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return null;
    } finally {
        $dbConnection = null;
    }
    return $customer;
}

/**
 * Get a total count of the customers by a specific email
 *
 * @param $dbConnection
 * @param $logger
 * @param $email
 * @return int
 */
function getTotalCustomerCountByEmail($dbConnection, $logger, $email){

    $count = 0;

    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/get_total_customer_count_by_email.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(['email' => $email]);

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        } else {
            $count = $stmt->fetchColumn();
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return $count;

}


/**
 * Inserts a customer into the database
 *
 * @param $dbConnection
 * @param $logger
 * @param $customer
 * @return int
 */
function createCustomer($dbConnection, $logger, $customer){
    try {
        $sqlQuery = file_get_contents(__DIR__ . '/statements/insert_customer.sql');

        $logger->info("Retrieved and going to execute the following query: ". $sqlQuery);

        $stmt = $dbConnection->prepare($sqlQuery);
        $stmt->execute(
            $customer
        );

        if(!$stmt){
            $logger->error("Database Error: ". $stmt->errorInfo() );
            return -1;
        }

    } catch(PDOException $exception){
        $logger->error("Database exception: ". $exception->getMessage());
        return -1;
    } finally {
        $dbConnection = null;
    }
    return 0;
}