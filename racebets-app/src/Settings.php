<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 03/09/2017
 * Time: 19:39
 */

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production

        // Monolog settings
        'logger' => [
            'name' => 'racebets-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database settings
        'database' => [
            'host' => 'localhost',
            'name' => 'racebets',
            'user' => 'racebets',
            'password' => 'racebets'
        ]
    ],
];