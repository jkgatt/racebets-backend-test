<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 05/09/2017
 * Time: 14:40
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once  __DIR__ . '/../database/account.php';
require_once  __DIR__ . '/../database/transaction_type.php';
require_once __DIR__ . '/helpers/validations.php';

// Add a deposit to the account of a customer
$app->post('/transaction', function(Request $request, Response $response){
    $parsedBody = $request->getParsedBody();

    // Validating the request to make sure that all the data required was passed in the body
    $validation = validateCreateTransactionRequest($parsedBody);

    if(!is_null($validation)) {
        return $response->withJson(
            array('error' => 'An error has occurred while trying to parse the body: ' . $validation . ''),
            400);
    }

    // Validating that the transaction type exists
    $transactionTypeId = getTransactionIdByName($this->database, $this->logger, $parsedBody['type']);
    if($transactionTypeId == -1){
        return $response->withJson(
            array('error' => 'Couldn\'t find the type('.$parsedBody['type'].') in the database...'),
            400);
    }


    // Validating that the customer ID exists
    $customer = getCustomerById($this->database, $this->logger, $parsedBody['customer_id']);

    if(is_null($customer) || !$customer){
        return $response->withJson(
            array('error' => 'Couldn\'t find the customer with the specified ID in the database...'),
            500);
    }

    // Validating the amount passed
    if($parsedBody['amount'] < 0){
        return $response->withJson(
            array('error' => 'The amount('.$parsedBody['amount'].') cannot be less than 0...'),
            400);
    }

    if(transactionToAccount($this->database, $this->logger, $transactionTypeId,
            $customer->bonus_percentage, $parsedBody) == -1){
        return $response->withJson(
            array('error' => 'Failed to add the new transaction to the database, please check the log for more details'),
            500);
    }
    return $response->withStatus(200);
});