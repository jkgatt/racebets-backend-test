<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 04/09/2017
 * Time: 10:03
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once  __DIR__ . '/../database/general.php';
require_once  __DIR__ . '/../database/customer.php';
require_once  __DIR__ . '/../database/gender.php';
require_once __DIR__ . '/helpers/validations.php';

// Get All Countries
$app->get('/customer', function(Request $request, Response $response) {
    $this->logger->info("Retrieving all the customers from the database");

    $customers = getAllCustomer($this->database, $this->logger);

    if(is_null($customers)){
        return $response->withJson(
            array('error' => 'An error has occurred when trying to retrieve data' .
            'from the database, please check the log files'),
            500);
    } else {
        return $response->withJson($customers, 200);
    }
});

// Create a new Customer in the database
$app->post('/customer', function(Request $request, Response $response){
    $parsedBody = $request->getParsedBody();

    // Validating the request to make sure that all the data required was passed in the body
    $validation = validateCreateCustomerRequest($parsedBody);

    if(!is_null($validation)){
        return $response->withJson(
            array('error' => 'An error has occurred while trying to parse the body: '. $validation .''),
            400);
    }

    // Checking if the user with the e-mail passed hasn't been registered yet.
    $customerCount = getTotalCustomerCountByEmail($this->database, $this->logger, $parsedBody['email']);
    if($customerCount != 0) {
        return $response->withJson(
            array('error' => 'A user with the following e-mail('.$parsedBody['email'].') has already been registered...'),
            500);
    }

    // Retrieving the gender ID for the specified gender
    $genderID = getGenderIdByName($this->database, $this->logger, $parsedBody['gender']);
    if($genderID == -1 || empty($genderID)){
        return $response->withJson(
            array('error' => 'The following gender ('.$parsedBody['gender'].') was not found in the database or an error ' .
             'occurred when trying to retrieve it from the database...'),
            500);
    }

    // Retrieving the country ID for the specified country
    $countryID = getCountryIdByName($this->database, $this->logger, $parsedBody['country']);
    if($countryID == -1 || empty($countryID)){
        return $response->withJson(
            array('error' => 'The following country ('.$parsedBody['country'].') was not found in the database or an error ' .
                'occurred when trying to retrieve it from the database...'),
            500);
    }
    // Randomize bonus percentage
    $bonusPercentage = rand (5, 20);

    $customer =  array(
        "first_name"        => $parsedBody['first_name'],
        "last_name"         => $parsedBody['last_name'],
        "email"             => $parsedBody['email'],
        "bonus_percentage"  => $bonusPercentage,
        "gender"            => $genderID,
        "country"           => $countryID
    );

    foreach( $customer as $key=>$value){
        $this->logger->info($key." -> ".$value);
    }


    // Inserting the customer into the database
    if(createCustomer($this->database, $this->logger,$customer) != 0){
        return $response->withJson(
            array('error' => 'An error has occured when trying to insert the new customer into the database (check log file)...'),
            500);
    }
    return $response->withStatus(200);
});

// Retrieve customer information via their database ID
$app->get('/customer/{id}', function(Request $request, Response $response, $arguments){

    // Reading the arguments
    $customerId = (int) $arguments['id'];

    // Retrieving the customer from the database
    $customer = getCustomerById($this->database, $this->logger, $customerId);

    if(is_null($customer)){
        return $response->withJson(
            array('error' => 'An error has occurred when trying to retrieve data' .
                'from the database, please check the log files'),
            500);
    } else{
        return $response->withJson($customer, 200);
    }
});

// Update customer information
$app->put('/customer/{id}', function(Request $request, Response $response, $arguments){

    // Reading the arguments
    $customerId = (int) $arguments['id'];

    $parsedBody = $request->getParsedBody();

    if(is_null($parsedBody)){
        return $response->withJson(
            array('error' => 'No JSON body was sent with the request'),
            400);
    }

    // Checking to see that the customer with the passed ID exists
    $customer = getCustomerById($this->database, $this->logger, $customerId);

    if(is_null($customer) || !$customer){
        return $response->withJson(
            array('error' => 'Didn\'t find the customer with the specified ID in the database or a database error occured'),
            500);
    }

    $updateQuery = "UPDATE `customer` SET";

    $totalSize = count($parsedBody);
    $counter = 0;

    // Looping over associative array
    foreach($parsedBody as $key => $value){
        if(empty($key) || empty($value)){
            return $response->withJson(
                array('error' => 'Recieved an invalid or empty, property and/or value...'),
                400);
        }
        switch($key){
            case 'first_name':
                $updateQuery .= " `first_name` = '" .$value. "'";
                break;
            case 'last_name':
                $updateQuery .= " `last_name` = '" .$value. "'";
                break;
            case 'email':
                // Checking if the email is unique
                $customerCount = getTotalCustomerCountByEmail($this->database, $this->logger, $value);
                if($customerCount != 0) {
                    return $response->withJson(
                        array('error' => 'A user with the following e-mail('.$value.') has already been registered...'),
                        500);
                }
                $updateQuery .= " `email` = '" .$value. "'";
                break;
            case 'gender':
                // Retrieving the gender ID for the specified gender
                $genderID = getGenderIdByName($this->database, $this->logger, $parsedBody['gender']);
                if($genderID == -1 || empty($genderID)){
                    return $response->withJson(
                        array('error' => 'The following gender ('.$value.') was not found in the database or an error ' .
                            'occurred when trying to retrieve it from the database...'),
                        500);
                }
                $updateQuery .= " `gender` = '" .$genderID. "'";
                break;
            case 'country':
                // Retrieving the country ID for the specified country
                $countryID = getCountryIdByName($this->database, $this->logger, $parsedBody['country']);
                if($countryID == -1 || empty($countryID)){
                    return $response->withJson(
                        array('error' => 'The following country ('.$value.') was not found in the database or an error ' .
                            'occurred when trying to retrieve it from the database...'),
                        500);
                }
                $updateQuery .= " `country` = '" .$countryID. "'";
                break;
            default:
                return $response->withJson(
                    array('error' => 'Recieved an invalid property, which the customer doesn\'t have...'),
                    400);
                break;
        }
        $counter++;
        if($counter < $totalSize){
            $updateQuery .= ",";
        }
    }

    $updateQuery .= " WHERE `id` = ".$customerId.";";


    // Executing the update query created above
    if(executeCustomQuery($this->database, $this->logger, $updateQuery) != 0){
        return $response->withJson(
            array('error' => 'An error has occured when trying to update a customer (check log file)...'),
            500);
    }
    return $response->withStatus(200);
});