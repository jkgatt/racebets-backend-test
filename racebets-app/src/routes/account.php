<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 06/09/2017
 * Time: 09:21
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once  __DIR__ . '/../database/account.php';
require_once __DIR__ . '/helpers/ReportData.php';

// Get All Countries
$app->get('/report', function(Request $request, Response $response){
    $this->logger->info("Retrieving the financial report from the database");

    $databaseReportData = getFinancialReport($this->database, $this->logger);

    if(is_null($databaseReportData)){
        return $response->withJson(
            array('error' => 'An error has occured when trying to retrieve data' .
                'from the database, please check the log files'),
            500);
    } else {
        $formattedReport = array();

        foreach($databaseReportData as $reportData){

            $formattedReportFound = false;
            $reportObject = null;

            // Checking to see if there is a report value that contains the same date and country in the formatted report
            foreach($formattedReport as $value){
                if($value->getDate() == $reportData->date && $value->getCountry() == $reportData->country){
                    $reportObject = $value;
                    $formattedReportFound = true;
                    break;
                }
            }

            if(!$formattedReportFound){
                $reportObject = new ReportData($reportData->date, $reportData->country);
            }

            // Adding or Updating the data in the reportObject
            if($reportData->transaction_type == "deposit") {
                $reportObject->setTotalDeposits($reportObject->getTotalDeposits() + $reportData->transaction_count);
                $reportObject->setTotalDepositAmount($reportObject->getTotalDepositAmount() + $reportData->amount);
            } else if($reportData->transaction_type == "withdrawal"){
                $reportObject->setTotalWithdrawals($reportObject->getTotalWithdrawals() + $reportData->transaction_count);
                $reportObject->setTotalWithdrawalAmount($reportObject->getTotalWithdrawalAmount() + $reportData->amount);
            }

            if(!$formattedReportFound) {
                array_push($formattedReport, $reportObject);
            }
        }

        return $response->withJson($formattedReport, 200);
    }
});