<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 03/09/2017
 * Time: 13:41
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once  __DIR__ . '/../database/country.php';

// Get All Countries
$app->get('/country', function(Request $request, Response $response){
    $this->logger->info("Retrieving all the countries from the database");

    $countries = getAllCountries($this->database, $this->logger);

    if(is_null($countries)){
        return $response->withJson(
            array('error' => 'An error has occured when trying to retrieve data' .
                'from the database, please check the log files'),
            500);
    } else {
        return $response->withJson($countries, 200);
    }
});