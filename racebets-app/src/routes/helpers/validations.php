<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 05/09/2017
 * Time: 08:56
 */

/**
 * Validates the JSON body of a create customer request
 *
 * @param $parsedBody
 * @return null|string
 */
function validateCreateCustomerRequest($parsedBody){

    if(is_null($parsedBody)){
        return "No JSON was found in the body of the request...";
    }

    if(empty($parsedBody['first_name'])){
        return "The first_name field wasn't found in the body of the request...";
    }

    if(empty($parsedBody['last_name'])){
        return "The last_name field wasn't found in the body of the request...";
    }

    if(empty($parsedBody['email'])){
        return "The email field wasn't found in the body of the request...";
    }

    if(empty($parsedBody['gender'])){
        return "The gender field wasn't found in the body of the request...";
    }

    if(empty($parsedBody['country'])){
        return "The country field wasn't found in the body of the request...";
    }

    return null;
}

/**
 * Validates the JSON body of a create transaction request
 *
 * @param $parsedBody
 * @return null|string
 */
function validateCreateTransactionRequest($parsedBody)
{

    if (is_null($parsedBody)) {
        return "No JSON was found in the body of the request...";
    }

    if (empty($parsedBody['type'])) {
        return "The type field wasn't found in the body of the request...";
    }

    if (empty($parsedBody['customer_id'])) {
        return "The customer_id field wasn't found in the body of the request...";
    }

    if(empty($parsedBody['amount'])){
        return "The amount field wasn't found in the body of the request...";
    }

    return null;
}