<?php

/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 06/09/2017
 * Time: 09:32
 */
class ReportData implements JsonSerializable
{
    private $date;
    private $country;
    private $total_deposits;
    private $total_deposit_amount;
    private $total_withdrawals;
    private $total_withdrawal_amount;

    /**
     * reportData constructor.
     * @param $date
     * @param $country
     */
    public function __construct($date, $country)
    {
        $this->date = $date;
        $this->country = $country;
        $this->total_deposits = 0;
        $this->total_deposit_amount = 0;
        $this->total_withdrawals = 0;
        $this->total_withdrawal_amount = 0;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getTotalDeposits()
    {
        return $this->total_deposits;
    }

    /**
     * @param mixed $total_deposits
     */
    public function setTotalDeposits($total_deposits)
    {
        $this->total_deposits = $total_deposits;
    }

    /**
     * @return mixed
     */
    public function getTotalDepositAmount()
    {
        return $this->total_deposit_amount;
    }

    /**
     * @param mixed $total_deposit_amount
     */
    public function setTotalDepositAmount($total_deposit_amount)
    {
        $this->total_deposit_amount = $total_deposit_amount;
    }

    /**
     * @return mixed
     */
    public function getTotalWithdrawals()
    {
        return $this->total_withdrawals;
    }

    /**
     * @param mixed $total_withdrawals
     */
    public function setTotalWithdrawals($total_withdrawals)
    {
        $this->total_withdrawals = $total_withdrawals;
    }

    /**
     * @return mixed
     */
    public function getTotalWithdrawalAmount()
    {
        return $this->total_withdrawal_amount;
    }

    /**
     * @param mixed $total_withdrawal_amount
     */
    public function setTotalWithdrawalAmount($total_withdrawal_amount)
    {
        $this->total_withdrawal_amount = $total_withdrawal_amount;
    }
}