<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 03/09/2017
 * Time: 20:06
 */

$container = $app->getContainer();

// Monolog Logger
$container['logger']= function ($c) {

    $loggerSettings = $c->get('settings')['logger'];

    $logger = new \Monolog\Logger($loggerSettings['name']);

    $logger->pushHandler(new Monolog\Handler\StreamHandler($loggerSettings['path'],
        $loggerSettings['level']));

    return $logger;
};

// Database
$container['database'] = function($c) {
    $dbSettings = $c->get('settings')['database'];

    $dbConnection = null;

    $sqlConnectStr = 'mysql:host='.$dbSettings['host'].';dbname='.$dbSettings['name'].'';

    try {
        $dbConnection = new PDO(
            $sqlConnectStr,
            $dbSettings['user'],
            $dbSettings['password']
        );
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbConnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $exception){
        echo '{"error": {"message": "Database connection init error: '. $exception->getMessage() . '"}}';
        die();
    }
    return $dbConnection;
};