<?php
/**
 * Created by PhpStorm.
 * User: jeangatt
 * Date: 03/09/2017
 * Time: 13:06
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies (database and logging mainly)
require __DIR__ . '/../src/dependencies.php';

// Setting up the route
require '../src/routes/country.php';
require '../src/routes/customer.php';
require '../src/routes/account.php';
require '../src/routes/transaction.php';

$app->run();