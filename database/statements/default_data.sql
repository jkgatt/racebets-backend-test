INSERT INTO `racebets`.`country`
	(`name`, 
    `short_name`)
VALUES
	("malta", "MT"),
    ("italy", "IT"),
    ("united kingdom", "UK"),
    ("germany", "DE");
    
INSERT INTO `racebets`.`gender`
	(`name`)
VALUES
	("male"),
    ("female"),
    ("other");
    
INSERT INTO `racebets`.`transaction_type`
	(`name`)
VALUES
	("deposit"),
    ("withdrawal");

   
